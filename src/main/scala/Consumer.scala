import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

object Consumer {

    def main(args: Array[String]): Unit = {
        val spark = SparkSession.builder
            .appName("Dynamas")
            .master("local[*]")
            .getOrCreate()

        spark.sparkContext.setLogLevel("WARN")
        import spark.implicits._

        val sdfKafkaTweets = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", "localhost:9092")
            .option("subscribePattern", "Stock.*")
            .option("startingOffsets", "latest")
            .load()
            .selectExpr("CAST(value AS STRING)")

        val tweetSchema = StructType(Seq(
            StructField("created_at", TimestampType, nullable=false),
            StructField("id", LongType, nullable=false),
            StructField("user_name", StringType, nullable=false),
            StructField("user_screen_name", StringType, nullable=false),
            StructField("user_favourites_count", IntegerType, nullable=false),
            StructField("user_followers_count", IntegerType, nullable=false),
            StructField("user_friends_count", IntegerType, nullable=false),
            StructField("user_statuses_count", IntegerType, nullable=false),
            StructField("quote_count", IntegerType, nullable=false),
            StructField("reply_count", IntegerType, nullable=false),
            StructField("retweet_count", IntegerType, nullable=false),
            StructField("favorite_count", IntegerType, nullable=false),
            StructField("text", StringType, nullable=false)
        ))

        val sdfStockTweets = sdfKafkaTweets
            .select(
                from_json(col("value"), tweetSchema).alias("parsedTweets")
            ).select("parsedTweets.*")
            .writeStream
            .format("console")
            .outputMode("append")
            .start()

        spark.streams.awaitAnyTermination()
    }
}
